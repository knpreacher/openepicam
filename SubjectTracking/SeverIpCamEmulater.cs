﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SubjectTracking
{
    public class SeverIpCamEmulater
    {
        public SeverIpCamEmulater()
        {
            HttpListener listener = new HttpListener();
            
            listener.Prefixes.Add("http://localhost:8888/connection/");
            listener.Start();
            
            HttpListenerContext context = listener.GetContext();
            HttpListenerRequest request = context.Request;
            
            HttpListenerResponse response = context.Response;
            
            string responseStr = "<html><head><meta charset='utf8'></head><body>Привет мир!</body></html>";
            byte[] buffer = Encoding.UTF8.GetBytes(responseStr);
            
            response.ContentLength64 = buffer.Length;
            Stream output = response.OutputStream;
            output.Write(buffer, 0, buffer.Length);
            
            output.Close();
            listener.Stop();
        }
    }
}
