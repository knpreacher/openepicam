﻿using System;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using Emgu.CV.Util;
using System.Windows.Forms;
using System.Drawing;
using System.Drawing.Imaging;

namespace SubjectTracking
{
    public class TestClass
    {
        Point infinity = new Point(int.MaxValue, int.MaxValue); // Точка, далеко за пределами экрана
        /// <summary>
        /// Равенство точки infinity означает, что объект не найден
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        bool IsInfinity(Point p)
        {
            return p.X == infinity.X && p.Y == infinity.Y;
        }

        /* Refs
         https://ru.stackoverflow.com/questions/521239/%D0%9E%D0%B1%D0%BD%D0%B0%D1%80%D1%83%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5-%D0%B7%D0%BE%D0%BD-%D0%BF%D1%80%D0%B8-%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D0%BE%D0%B2%D0%B0%D0%BD%D0%B8%D0%B8-%D0%BF%D0%BE%D1%80%D0%BE%D0%B3%D0%B0
         http://www.cyberforum.ru/windows-forms/thread2282102.html
         https://www.codeproject.com/Tips/616803/Binarizing-image-Thresholding-using-EmguCV
         https://stackoverflow.com/questions/22735017/opencv-bitwise-and-function-in-emgu
         */
        /*public void HelloWorld()
        {
            string win1 = "Test Window";                    // The name of the window
            CvInvoke.NamedWindow(win1);                     // Create the window using the specific name

            Mat img = new Mat(200, 400, DepthType.Cv8U, 3); // Create a 3 channel image of 400x200
            img.SetTo(new Bgr(255, 0, 0).MCvScalar);        // Set it to Blue color

            CvInvoke.PutText(                               // Draw "Hello, world" on the image using the specific font
               img,
               "Hello, world",
               new System.Drawing.Point(10, 80),
               FontFace.HersheyComplex,
               1.0,
               new Bgr(0, 255, 0).MCvScalar);


            CvInvoke.Imshow(win1, img);                     // Show the image
            CvInvoke.WaitKey(0);                            // Wait for the key pressing event
            CvInvoke.DestroyWindow(win1);                   // Destroy the window if key is pressed
        }*/
        //class CameraSearch
        //{
        //    public bool searchFinished = true;

        //    Camera camera;
        //    int xbCount;
        //    int ybCount;
        //    int dx;
        //    int dy;
        //    int border = 10; // Граница окна поиска; разница между шириной камеры и шириной окна поиска
        //    Point center;
            

        //    public CameraSearch(Camera camera)
        //    {
        //        this.camera = camera;

        //        dx = camera.size.Width - border;
        //        dy = camera.size.Height - border;

        //        // Делим окна на блоки размером с камеру минус граница
        //        xbCount = camera.screenRectangle.Width / dx + 1; // Количество блоков по x
        //        ybCount = camera.screenRectangle.Height / dy + 1; // Количество блоков по y

        //        center = new Point((xbCount / 2 + 1) * dx, (ybCount / 2 + 1) * dy);
        //    }
        //    /// <summary>
        //    /// Поиск маркера камерой; используется алгоритм поиска в ширину от центра экрана
        //    /// </summary>
        //    public void SearchTarget()
        //    {
        //        searchFinished = false;

        //        if (!IsCenter(camera.position))
        //            camera.Move(center);
        //    }
        //    bool IsCenter(Point p)
        //    {
        //        return p.X == center.X && p.Y == center.Y;
        //    }
        //}

        class Camera
        {
            public Size size;
            public Point position;
            public Rectangle screenRectangle;
            //public CameraSearch cameraSearch;

            readonly int pxPerUpdate = 5;
            public Camera()
            {
                screenRectangle = Screen.AllScreens[0].Bounds;
                //screenRectangle.Width /= 2;
                size = new Size(screenRectangle.Width, screenRectangle.Height);
                position = new Point();

                //cameraSearch = new CameraSearch(this);
            }
            /// <summary>
            /// Движение камеры в указанную точку
            /// </summary>
            /// <param name="relativeP">Координаты относительно положения камеры (левого верхнего угла окна)</param>
            public void Move(Point relativeP)
            {
                //var p = new Point(position.X + relativeP.X, position.Y + relativeP.Y);
                var p = new Point(
                    position.X + Math.Sign(relativeP.X) * Math.Min(pxPerUpdate, Math.Abs(relativeP.X)),
                    position.Y + Math.Sign(relativeP.Y) * Math.Min(pxPerUpdate, Math.Abs(relativeP.Y)));

                if (p.X >= 0 && p.X < screenRectangle.Width - size.Width)
                    position.X = p.X;
                if (p.Y >= 0 && p.Y < screenRectangle.Height - size.Height)
                    position.Y = p.Y;
            }
        }

        public void ScreenCapture(ImageBox imageBox)
        {
            Rectangle captureRectangle = Screen.AllScreens[0].Bounds;
            Bitmap captureBitmap = new Bitmap(captureRectangle.Width, captureRectangle.Height, PixelFormat.Format32bppArgb);
            Graphics captureGraphics = Graphics.FromImage(captureBitmap);


            //Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
            Application.Idle += new EventHandler((object sender, EventArgs e) =>
            {
                captureGraphics.CopyFromScreen(captureRectangle.Left, captureRectangle.Top, 0, 0, captureRectangle.Size);

                Image<Bgr, byte> img = new Image<Bgr, byte>(captureBitmap);
                Point p = DetectMarker(ref img, true);
                OutTarget(ref img, p);

                
                imageBox.Image = img;
                img.Dispose();
            });
        }
        public void BoxCapture(ImageBox imageBox)
        {
            Camera camera = new Camera();

            Bitmap captureBitmap = new Bitmap(camera.size.Width, camera.size.Height, PixelFormat.Format32bppArgb);
            Graphics captureGraphics = Graphics.FromImage(captureBitmap);
            Application.Idle += new EventHandler(delegate (object sender, EventArgs e)
            {
                captureGraphics.CopyFromScreen(camera.position.X, camera.position.Y, 0, 0, camera.size);

                Image<Bgr, byte> img = new Image<Bgr, byte>(captureBitmap);
                Point p = DetectMarker(ref img, true);
                
                if (p.X != 0 || p.Y != 0) // TODO: Поменять на p.IsEmpty()
                {
                    if (!IsInfinity(p))
                    {
                        //camera.cameraSearch.searchFinished = true;

                        OutTarget(ref img, p);
                        p = new Point(p.X - camera.size.Width / 2, p.Y - camera.size.Height / 2); // TODO: Поменять на p.Offset(...)
                        camera.Move(p);
                    }
                    else
                    {
                        //camera.cameraSearch.SearchTarget();
                    }
                }

                imageBox.Image = img;
                img.Dispose();
            });
        }
        Point DetectMarker(ref Image<Bgr, byte> image, bool outTargetContour)
        {
            // Params
            Hsv lowerLimit = new Hsv(31, 191, 191);
            Hsv upperLimit = new Hsv(71, 255, 255);
            const int threshMin = 191;
            const int threshMax = 255;


            // Find colors
            Image<Gray, byte> mask = image.Convert<Hsv, byte>().InRange(lowerLimit, upperLimit);
            Image<Gray, byte> binMask = mask.ThresholdBinary(new Gray(threshMin), new Gray(threshMax));

            // Find contours
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat hierarchy = new Mat();
            CvInvoke.FindContours(binMask, contours, hierarchy, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            // Find the biggest contour and return it's center
            if (contours.Size > 0)
            {
                int maxAreaIndex = 0;
                double maxArea = CvInvoke.ContourArea(contours[maxAreaIndex]);
                for (int i = 1; i < contours.Size; i++)
                {
                    var a = CvInvoke.ContourArea(contours[i]);
                    if (a > maxArea)
                    {
                        maxAreaIndex = i;
                        maxArea = a;
                    }
                }
                Rectangle rc = CvInvoke.BoundingRectangle(contours[maxAreaIndex]);

                if (outTargetContour)
                    OutTargetCountour(ref image, contours, maxAreaIndex);

                return new Point(rc.X + rc.Width / 2, rc.Y + rc.Height / 2);
            }

            return infinity;
        }
        /// <summary>
        /// Выделение контура цели на кадре
        /// </summary>
        /// <param name="image">Кадр</param>
        /// <param name="contours">Контуры</param>
        /// <param name="index">Индекс цели в массиве контуров</param>
        void OutTargetCountour(ref Image<Bgr, byte> image, VectorOfVectorOfPoint contours, int index)
        {
            //CvInvoke.DrawContours(imageHsv, contours, -1, targetColor); // -1 to draw all contours
            MCvScalar targetColor = new MCvScalar(0, 0, 255);
            CvInvoke.DrawContours(image, contours, index, targetColor);
        }
        /// <summary>
        /// Выделение цели на кадре
        /// </summary>
        /// <param name="image">Кадр</param>
        /// <param name="p">Цель</param>
        void OutTarget(ref Image<Bgr, byte> image, Point p)
        {
            const int targetSize = 10;
            MCvScalar targetColor = new MCvScalar(0, 0, 255);
            CvInvoke.Circle(image, p, targetSize, targetColor, 2 * targetSize);
        }
    }
}
