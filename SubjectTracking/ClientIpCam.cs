﻿using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.UI;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SubjectTracking
{
    class ClientIpCam
    {
        const string URL = "http://192.168.2.51:3000/image";
        ImageBox imageBox;
        SubjectRecognition sr;

        public ClientIpCam(ImageBox imageBox)
        {
            this.imageBox = imageBox;
            sr = new SubjectRecognition();

            //Task t = new Task(() => 
            //{ 

            //});
            //t.Start();
            
            Task.Run(RequestAsync);
            
        }

        private async Task RequestAsync()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(URL);
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();

            using (Stream stream = response.GetResponseStream())
            {
                var img = new Image<Bgr, byte>(new Bitmap(stream));
                sr.OutTarget(ref img, sr.DetectMarker(ref img, true));
                imageBox.Image = img;
            }
            response.Close();

            await RequestAsync();
        }
    }
}
