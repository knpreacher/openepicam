﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.CV.Util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SubjectTracking
{
    class SubjectRecognition
    {
        Point infinity = new Point(int.MaxValue, int.MaxValue); // Точка, далеко за пределами экрана
        /// <summary>
        /// Равенство точки infinity означает, что объект не найден
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        bool IsInfinity(Point p)
        {
            return p.X == infinity.X && p.Y == infinity.Y;
        }

        public Point DetectMarker(ref Image<Bgr, byte> image, bool outTargetContour)
        {
            // Params
            Hsv lowerLimit = new Hsv(31, 191, 191);
            Hsv upperLimit = new Hsv(71, 255, 255);
            const int threshMin = 191;
            const int threshMax = 255;


            // Find colors
            Image<Gray, byte> mask = image.Convert<Hsv, byte>().InRange(lowerLimit, upperLimit);
            Image<Gray, byte> binMask = mask.ThresholdBinary(new Gray(threshMin), new Gray(threshMax));

            // Find contours
            VectorOfVectorOfPoint contours = new VectorOfVectorOfPoint();
            Mat hierarchy = new Mat();
            CvInvoke.FindContours(binMask, contours, hierarchy, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            // Find the biggest contour and return it's center
            if (contours.Size > 0)
            {
                int maxAreaIndex = 0;
                double maxArea = CvInvoke.ContourArea(contours[maxAreaIndex]);
                for (int i = 1; i < contours.Size; i++)
                {
                    var a = CvInvoke.ContourArea(contours[i]);
                    if (a > maxArea)
                    {
                        maxAreaIndex = i;
                        maxArea = a;
                    }
                }
                Rectangle rc = CvInvoke.BoundingRectangle(contours[maxAreaIndex]);

                if (outTargetContour)
                    //OutTargetCountour(ref image, contours, maxAreaIndex);
                    OutAllCountours(ref image, contours);


                return new Point(rc.X + rc.Width / 2, rc.Y + rc.Height / 2);
            }

            return infinity;
        }

        void OutTargetCountour(ref Image<Bgr, byte> image, VectorOfVectorOfPoint contours, int index)
        {
            //CvInvoke.DrawContours(imageHsv, contours, -1, targetColor); // -1 to draw all contours
            MCvScalar targetColor = new MCvScalar(0, 0, 255);
            CvInvoke.DrawContours(image, contours, index, targetColor);
        }
        void OutAllCountours(ref Image<Bgr, byte> image, VectorOfVectorOfPoint contours)
        {
            MCvScalar targetColor = new MCvScalar(0, 0, 255);
            CvInvoke.DrawContours(image, contours, -1, targetColor);
        }
        /// <summary>
        /// Выделение цели на кадре
        /// </summary>
        /// <param name="image">Кадр</param>
        /// <param name="p">Цель</param>
        public void OutTarget(ref Image<Bgr, byte> image, Point p)
        {
            const int targetSize = 10;
            MCvScalar targetColor = new MCvScalar(0, 0, 255);
            CvInvoke.Circle(image, p, targetSize, targetColor, 2 * targetSize);
        }
    }
}
